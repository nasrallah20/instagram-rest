package servlet;

import model.Post;
import model.User;
import org.apache.commons.io.IOUtils;
import client.PostService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;


@WebServlet(name = "AddPost", urlPatterns = "/addPost")
@MultipartConfig(location="/tmp", fileSizeThreshold=1024*1024,
        maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)

public class AddPostServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération du user et de la descritpion
        HttpSession session = request.getSession();
        User userConnected = (User) session.getAttribute("userConnected");
        String description = request.getParameter("description");

        //récupération de la date
        java.util.Date currentDate = Calendar.getInstance().getTime();
        java.sql.Date date = new java.sql.Date(currentDate.getTime());

        Part image = request.getPart("image");
        InputStream fileContent = image.getInputStream();

        byte[] bytes = IOUtils.toByteArray(fileContent);

        Post post = new Post();
        post.setUser(userConnected);
        post.setFileimage(bytes);
        post.setDescription(description);

        PostService.addPost(post);

        response.sendRedirect("posts");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher disp;
        disp = request.getRequestDispatcher("/addPost.jsp");
        disp.forward(request, response);
    }
}
