package servlet;

import model.Comment;
import model.Post;
import model.User;
import client.CommentService;
import client.PostService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

@WebServlet(name = "CommentServlet", urlPatterns = "/addComment")

public class AddCommentServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User userConnected = (User) session.getAttribute("userConnected");

        RequestDispatcher disp;
        String post_id = (String) session.getAttribute("post_id");
        Post post =  PostService.getPostById(Integer.parseInt(post_id));

        String comment = request.getParameter("comment");

        Comment newComment = new Comment();
        newComment.setUser(userConnected);
        newComment.setPost(post);

        newComment.setComment(comment);

        boolean res = CommentService.addComment(newComment, Integer.parseInt(post_id));



        if (res == false){
            request.setAttribute("commentAlert", "Add comment fail");
            disp = request.getRequestDispatcher("/addComment.jsp");
            disp.forward(request, response);
        }else{
            response.sendRedirect("posts");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher disp;
        String post_id = request.getParameter("post_id");
        session.setAttribute("post_id", post_id);
        disp = request.getRequestDispatcher("/addComment.jsp");
        disp.forward(request, response);



    }
}
