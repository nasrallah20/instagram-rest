package client;


import model.Post;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Base64;
import java.util.List;

public class PostService {

    private static String URL = "http://localhost:8080/server/api";

    private static Client client = ClientBuilder.newClient();

    private static WebTarget target = client.target(URL);

    public static List<Post> getPosts(){

        GenericType<List<Post>> genericType = new GenericType<List<Post>>() {};

        List<Post> response = target.path("postService")
                .path("posts")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(genericType);

        for(Post post : response) {
            post.setBase64(Base64.getEncoder().encodeToString(post.getFileimage()));
        }
        return response;
    }

    public static Post getPostById(int idPost){
        Response response = target.path("postService")
                .path(String.valueOf(idPost))
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();

        Post post = response.readEntity(Post.class);
        return post;
    }

    public static boolean addPost(Post post){

        Response response = target.path("postService")
                .path("/addPost")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(post, MediaType.APPLICATION_JSON),
                Response.class);
        boolean result  = response.readEntity(boolean.class);

        System.out.println(result);
        return result;
    }
}
