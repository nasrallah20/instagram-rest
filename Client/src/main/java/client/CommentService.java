package client;

import javafx.geometry.Pos;
import model.Comment;
import model.Post;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class  CommentService {
    private static String URL = "http://localhost:8080/server/api";

    private static Client client = ClientBuilder.newClient();

    private static WebTarget target = client.target(URL);

    public static boolean addComment(Comment comment, int postId){

        Response response = target.path("commentService")
                .path("addComment")
                .path(String.valueOf(postId))
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(comment, MediaType.APPLICATION_JSON),
                Response.class);

        boolean result = response.readEntity(boolean.class);
        System.out.println(result);
        return result;
    }

}
