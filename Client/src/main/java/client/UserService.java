package client;

import model.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class UserService {

    private static String URL = "http://localhost:8080/server/api";

    private static Client client = ClientBuilder.newClient();

    private static WebTarget target = client.target(URL);

    public static User checkAuthentication(String username, String password){
        Response response = target.path("userService")
                .path("login")
                .path(username)
                .path(password)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();

        User user = response.readEntity(User.class);

        return user;
    }
}
