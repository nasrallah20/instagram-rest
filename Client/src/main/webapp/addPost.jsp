<%--
  Created by IntelliJ IDEA.
  User: nournasrallah
  Date: 28/05/2020
  Time: 00:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@600&display=swap" rel="stylesheet">"
<html>
<head>
    <title>Add Post</title>
</head>
<style>

    body {
        font-family: 'Raleway', sans-serif;
    }
</style>
<body class="text-center" style="margin-top: 10%">
<h5 style="color: darkmagenta; margin-top: 10px">Add your post</h5>

<form action="addPost" method="post" enctype='multipart/form-data'>
    <label>What's on your mind ! </label>
    <br>
    <input style="margin-top: 10px" type="text" name="description" placeholder="Description"/>
    <br>
    <input type="file" name="image" style="margin-top: 10px" />
    <br>
    <input style="margin-top: 10px; background-color: darkmagenta" type="submit"  class="btn btn-primary"/>

</form>

</body>
</html>

