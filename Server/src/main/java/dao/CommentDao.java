package dao;

import model.Comment;
import model.Post;

public class CommentDao extends DAO{


	public static boolean addComment(Comment comment){
		try{
			entityManager.getTransaction().begin();
			entityManager.persist(comment);
			entityManager.getTransaction().commit();
			return true;
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}
}
