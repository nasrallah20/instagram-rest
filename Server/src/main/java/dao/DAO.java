 package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DAO {
	protected static EntityManagerFactory entityManagerFactory =
	            Persistence.createEntityManagerFactory("instagram");
	  protected static EntityManager entityManager=entityManagerFactory.createEntityManager();
	
}
