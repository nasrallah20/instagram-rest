package dao;

import model.User;

import javax.persistence.TypedQuery;

public class UserDao extends DAO {

    public static User checkLogin(String username ,String password){
        User user = getUserById(username);
        if (user != null) {
            password.equals(user.getPassword());
            return user;
        }
        return null;
    }

    public static User getUserById(String username) {
        User user = new User();
        try{
            TypedQuery<User> query = entityManager.createQuery("select user from User user WHERE user.username=:username", User.class);
            query.setParameter("username", username);
            user = query.getSingleResult();
            return user;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
