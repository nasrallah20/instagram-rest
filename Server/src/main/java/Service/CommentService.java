package Service;

import dao.CommentDao;
import model.Comment;
import model.Post;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("commentService")
public class CommentService {

    @POST
    @Path("/addComment/{postId}")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean addComment(@PathParam("postId") int postId, Comment comment){
        Post post = PostService.getPost(postId);
        post.addComment(comment);
        return CommentDao.addComment(comment);
    }

}
