package Service;

import dao.PostDao;
import model.Post;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Base64;
import java.util.List;

@Path("postService")
public class PostService {

    @GET
    @Path("/posts")
    @Produces(MediaType.APPLICATION_JSON)
    public static List<Post> getPosts() {

        List<Post> posts = PostDao.getPosts();
        for(Post post : posts) {
            post.setBase64(Base64.getEncoder().encodeToString(post.getFileimage()));
        }
        return posts;
    }

    @GET
    @Path("/{postId}")
    @Produces(MediaType.APPLICATION_JSON)
    public static Post getPost(@PathParam("postId") int postId) {
        return PostDao.getPostById(postId);
    }

    @POST
    @Path("/addPost")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean addPost(Post post){
        return PostDao.addPost(post);
    }
}
