package Service;

import dao.UserDao;
import model.User;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("userService")
public class UserService {

    @GET
    @Path("/login/{username}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public static User login(@PathParam("username") String username, @PathParam("password") String password){
        return UserDao.checkLogin(username, password);
    }



}
